# What is Ditui ?

Ditui is a Discord Client in a Text-based user interface using [DiscordJS](https://discord.js.org),
[BlessedJS](https://github.com/chjj/blessed) and a bit of
[blessed-contrib](https://github.com/yaronn/blessed-contrib) (for the tree widget)

# Why did I make it ?

I find the official Discord client extremly consuming and I wanted a more lightweight one.
I looked up on the net and found some but they weren't very pratical so I decided to do mine.

# How to use it ?

1. Install [NodeJS](https://nodejs.org) and [npm](https://www.npmjs.com/)

	`apt-get install nodejs npm` (or whatever package manager you use)

2. Clone the repo

	`git clone https://gitlab.com/Noukkis/Ditui.git`

3. Go to the Directory and install the dependencies

	`npm install`

4. Find your discord Token

    * Open Discord (browser or Dekstop App)

    * Open the browser console ( `F12` or `Ctrl + Shift + I`)

    * Under the "Application" tab, look for "Storage", "Local Storage" and then the Discord URL

    * You see a list of variables, find the "token" one and copy it

		**Warning :** Don't let anyone access your token or they
		will be able to take control of your account. Don't trust anyone.

5. Under the "Ditui" directory, you'll find a "config.template.json", rename it to "config.json"
	and paste your token in it, remplacing "Your Discord Token".

6. (optional) You can modify the config as you please

7. Run the application with the file in the "bin" directory

# Control

You can choose the channel in the left pannel and chat in the main pannel.

There is a menu that you can open with the associated keyboard shortcut (default `Ctrl + G`)
this menu aloows you to choose a Guild or open your DMs.