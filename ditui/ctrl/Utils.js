'use-strict';

module.exports = {

	isUserMentioned(user, msg) {
		var mentions = msg.mentions;
		return mentions.everyone
		|| mentions.users && mentions.users.array().includes(user)
		|| this._isUserRoleMention(user, msg, mentions);
	},

	arrIntersect(target, toMatch) {
		var found, targetMap, i, j, cur;
		found = false;
		targetMap = {};
		for (i = 0, j = target.length; i < j; i++) {
			cur = target[i];
			targetMap[cur] = true;
		}
		for (i = 0, j = toMatch.length; !found && (i < j); i++) {
			cur = toMatch[i];
			found = !!targetMap[cur];
		}
		return found;
	},

	_isUserRoleMention(user, msg, mentions) {
		if(!msg.guild || !mentions.roles) return false;
		var member = msg.guild.member(user);
		if(!member) return false;
		return this.arrIntersect(mentions.roles.array(), member.roles.array());
	},

};
