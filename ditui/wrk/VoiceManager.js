'use-strict';

const stream = require('stream'),
	Speaker = require('speaker');

class VoiceManager {

	constructor() {
		this.receiver = null;
		this.bitstream = new stream.PassThrough();
		this.speaker = new Speaker();
		this.bitstream.pipe(this.speaker);
	}

	connect(conn) {
		this.disconnect();
		this.receiver = conn.createReceiver();
		this.receiver.on('pcm', (user, pcm) => this.bitstream.write(pcm));
	}

	disconnect() {
		if(this.receiver) {
			this.receiver.destroy();
			this.receiver = null;
		}
	}

}

module.exports = VoiceManager; 
