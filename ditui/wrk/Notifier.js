'use-strict';

const notifier = require('node-notifier')
	utils = require('../ctrl/Utils'),	
	ActiveWin = require('./ActiveWin');

class Notifier {

	constructor() {
		this.activeWin = new ActiveWin();
		this.activeWin.setup();
	}

	checkAndNotify(msg, user, curChan) {
		this._checkMustNotify(msg, user, curChan, mustNotify => {
			if(mustNotify) this._notify(msg);
		});
	}

	_checkMustNotify(msg, user, curChan, cb) {
		var mustNotif = !msg.channel.muted;
		var notifType = msg.channel.messageNotifications;
		if(msg.guild && mustNotif) {
			if(msg.guild.muted) {
				mustNotif = false;
			} else if(notifType == 'INHERIT') {
				notifType = msg.guild.messageNotifications;
			}
			if(!notifType || notifType == 'NOTHING') mustNotif = false;
			else if(notifType == 'EVERYTHING') mustNotif = true;
			else mustNotif = utils.isUserMentioned(user, msg);
		}
		if(mustNotif) {
			if(msg.channel == curChan) {
				this.activeWin.isActiveWin(active => cb(!active))
			} else {
				cb(true);
			}
		}
	}

	_notify(msg) {
		var name;
		if(msg.channel.type == 'dm') name = msg.channel.recipient.username;
		else if(msg.channel.type == 'group') name = msg.channel.recipients.array().map(u => u.username).join(' - ');
		else name = msg.guild.name + '|' + msg.channel.name;
		notifier.notify({
			title: name,
			message: msg.cleanContent
		});
	}

}

module.exports = Notifier;
