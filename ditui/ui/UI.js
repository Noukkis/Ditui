'use-strict';

const blessed = require('blessed'),
	EventEmitter = require('events'),
	tree = require('./Tree');

class UI extends EventEmitter {

	constructor(config, screenTitle, promptTitle, promptBtnName) {
		super();
		this.config = config;
		
		this.screen = blessed.screen({
			smartCSR: true,
			autoPadding: true,
			title: screenTitle
		});

		this.mainBox = blessed.box({
			top: 'center',
			left: 'center',
			height: '100%',
			width: '100%',
			border: {type: 'line'}
		});

		this.infoBox = blessed.box({
			tags: true,
			left: '15%+2',
			height: 'shrink',
			width: '85%-4',
			border: {type: 'line'}
		});

		this.leftMenu = tree({
			width: '15%',
			mouse: true,
			style : {
				selected: {inverse: true}
			}
		});
		this.leftMenu.append(blessed.line({left: '100%'}));

		this.chat = blessed.box({
			tags: true,
			top: '0%+3',
			left: '15%+2',
			height: '100%-8',
			width: '85%-4',
			keys: true,
			alwaysScroll: true,
			scrollable: true,
			scrollbar: {
			style: {
				bg: config.color.accent
			}
			},
			clickable: true,
		});

		this.chatReply = blessed.textbox({
			top: '100%-5',
			left: '15%+2',
			height: 'shrink',
			width: '85%-4',
			keys: true,
			mouse: true,
			inputOnFocus: true,
			border: {type: 'line'}
		});

		this.prompt = blessed.box({
			width: '50%',
			height: '50%',
			left: '25%',
			top: '25%',
			draggable: true,
			hidden: true,
			mouse:true,
			border: {type: 'line', fg: config.color.secondary}
		});
		this.prompt.append(blessed.line({orientation: 'horizontal', top: '0%+1', fg: config.color.secondary}));
		this.prompt.append(blessed.line({orientation: 'horizontal', top: '100%-4', fg: config.color.secondary}));

		this.promptList = blessed.list({
			parent: this.prompt,
			width: '100%-32',
			height: '100%-6',
			left: '0%+2',
			top: '0%+2',
			mouse: true,
			keys: true,
			mode: '',
			style : {
				selected: {inverse: true}
			}
		});

		this.promptTitle = blessed.text({
			content: promptTitle,
			parent: this.prompt,
			left: '0%+2',
			fg: config.color.secondary
		});

		this.cancelBtn = blessed.button({
			parent: this.prompt,
			content: '<cancel>',
			top: '100%-3',
			left: '100%-12',
			shrink: true,
			fg: 'red',
			bold: true
		})

		this.privateBtn = blessed.button({
			parent: this.prompt,
			content: '<' + promptBtnName + '>',
			top: '100%-3',
			left: '0%+1',
			shrink: true,
			fg: config.color.accent,
			bold: true
		})

		this.screen.append(this.mainBox);
	}

	setupListeners () {
		this.key(this.config.keys.exit, () => this._exit());
		this.chatReply.on('submit', () => {
			var content = this.chatReply.value;
			this.chatReply.clearValue();
			this.chatReply.focus();
			this.chat.setScrollPerc(100);
			this.update();
			this.emit('chatSubmit', content);
		});
		
		this.leftMenu.on('select', el => this.emit('leftMenuSelect', el));
		
		this.promptList.on('select', (el, index) => {
			this.prompt.hidden = true
			this.update();
			this.emit('promptSelect', el, index);
		});
		
		this.cancelBtn.on('click', () => {
			this.prompt.hidden=true;
			this.update();
		});
		
		this.privateBtn.on('click', () => {
			this.prompt.hidden=true;
			this.update();
			this.emit('promptBtnSelect');
		});
		
		this.chat.on('wheelup', () => {
			this.chat.scroll(-1);
			this.update();
		});

		this.chat.on('wheeldown', () => {
			this.chat.scroll(1);
			this.update();
		});

		this.chat.on('scroll', () => {
			if(this.chat.getScrollPerc() < 1) {
				this.emit('scrolledTop');
			}
		});
	}

	loading(text) {
		this.mainBox.children = []
		this.mainBox.append(blessed.text({
			tags: true,
			content: `{blink}${text}{/blink}`,
			top: 'center',
			left: 'center'
		}));
		this.update();
	}

	showMain() {
		this.mainBox.children = [];
		this.mainBox.append(this.leftMenu);
		this.mainBox.append(this.chat);
		this.mainBox.append(this.infoBox);
		this.mainBox.append(this.chatReply);
		this.mainBox.append(this.prompt);
		this.update();
	}

	key(key,  cb) {
		this.screen.key(key, cb);
		this.chatReply.key(key, cb);
	}

	_exit() {
		if(this.prompt.hidden) this.emit('exit');
		else {
			this.prompt.hidden = true;
			this.update();
		}
	}

	update() {
		this.screen.render();
	}

	escape(text) {
		return blessed.escape(text);
	}

	addLineChat(line) {
		var scroll = this.chat.getScrollPerc();
		this.chat.pushLine(line);
		if(scroll > 99) {
			this.chat.setScrollPerc(100);
		}
		this.update();
	}

	addMultipleLinesBeforeChat(lines) {
		var scrolled = this.chat.getScrollHeight();
		this.chat.insertLine(0, lines.reverse());
		var toScroll = this.chat.getScrollHeight() - scrolled;
		this.chat.scroll(toScroll);
		this.update();
	}

	addMultipleLinesChat(lines) {
		this.chat.insertLine(0, lines.reverse());
		this.chat.setScrollPerc(100);
		this.update();
	}

	openPrompt(items) {
		this.prompt.hidden = false;
		this.promptList.clearItems();
		for(let item of items) {
			this.promptList.add(item);
		}
		this.update();
	}

	set leftMenuData(data) {
		this.leftMenu.setData({extended: true, children: data});
		this.update();
	}

	set infoBoxTitle(title) {
		this.chat.setContent('');
		this.chat.deleteLine(0);
		this.infoBox.content = title;
		this.update();
	}

}

module.exports = UI;
